# Exploratory Files

This folder is the "lab bench" of the codebase. **Notebooks** (Jupyter, Zeppelin, etc.) **go here**. The name, while regrettably a collision with the exponential function `exp()`, conveys the following kinds of files in this directory:

- **Exploration**: Files that capture the exploration process when beginning a project. For data science projects, this is called [Exploratory Data Analysis](https://en.wikipedia.org/wiki/Exploratory_data_analysis) (or EDA). Output (like charts and graphs) should be checked in/included here, if possible, so new contributors to the codebase can quickly glean insights from the results of previous exploration work.
- **Experimentation**: Files relating to experimenting with dependencies, APIS, or code in `/src`. This can be thought of a "scratch" space for trying out something on an ad-hoc basis before it's ready to be formalized as a test or documentation.
- **Experiments**: A log of the experiments that were tried during the modeling process. You can prefix folder/file names with the ordinal (the experiment number) or date of the experiment to help organize them. Here is an example of what folders beneath the `/exp` folder could look like:
```
/exp
  /200414-lr
  /200416-convergence
  /200416-convergence-alldata
  /200422-cpu-timings
  /200427-gpu-timings
    /analysis.py
    /config.yaml
    /p2.xlarge-1.log
    /run-timings.sh
  /200428-infer-training-data
```

| :information_source: Promote code into `/src` |
| --- |
| If you are copying and pasting code from this folder to reuse it, or if you are using experimental code as part of a deliverable, the code is ready to be "promoted" and factored into a reusable package in [`/src`](../src/). |