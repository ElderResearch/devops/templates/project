# ERI Standard Project Layout

<img src="https://img.shields.io/badge/version-0.0.2-blue" border="0" alt="Version 0.0.2">

| :warning: This repository is publicly accessible so no proprietary or sensitive information should be committed |
| --- |

This repository has the top-level folder structure that all ERI projects and packages (every code repository) _should_ have[^1]. As always, this is a guideline, not a requirement. The goal of this standardization is two-fold:
- Technical staff are more quickly able to orient themselves to a new codebase and find what they're looking for as they move from repository to repository
- Since we use many programming languages and frameworks, this attempts to find a "common ground" and a way for heterogeneous projects[^2] to be organized in a unified way.

Please try to conform your project as much as possible, but the over-arching rule is that **if your programming language or build tool has a convention, follow that convention first**. This particularly applies to the organization under `/src`.

| :information_source: The ERI-CLI will bootstrap this folder structure for you when starting new repositories. |
| --- |
| In addition, it will initialize language- and built tool-specific files and folders, as well as initialize a repository for GitLab CI/CD. [Download the latest release here](https://gitlab.com/ElderResearch/devops/eri-cli/-/releases "Download the latest release here") and add it to your `PATH`.  |

<!-- This table is parsed by the ERI-CLI when creating new repos. Please keep the same structure. -->
| Folder | Description |
| --- | --- |
| [`/conf`](/conf) | Configuration & environment-specific files |
| [`/data`](/data) | Data files, access instructions, data dictionaries, reference files |
| [`/doc`](/doc) | Documentation, diagrams, tutorials |
| [`/exp`](/exp) | Experiments/exploration/ad-hoc analysis, notebooks |
| [`/lib`](/lib) | Native libraries, ad-hoc dependencies not handled by a build tool |
| [`/models`](/models) | Serialized/trained models |
| [`/src`](/src) | Source code, packages, test code (including SQL/DDL) |
| [`/target`](/target) | Compiled code, generated source code |
| [`/`](/root.md) | Root project files (README, build files, licenses) |

[^1]: This layout was partially inspired by [cookie cutter data science](https://drivendata.github.io/cookiecutter-data-science/), Linux conventions, and existing conventions across [JVM languages](https://maven.apache.org/guides/introduction/introduction-to-the-standard-directory-layout.html), Python, and R.

[^2]: Most projects should be _homogeneous_ with one language/build tool. If an overall system includes components with different languages and frameworks, consider separating those two components into different repositories (since they will likely evolve differently and involve different contributors). However, in rare cases, a single codebase will involve multiple languages and build tools, and this top-level organization should accommodate them in parallel. 