# Target Files

This folder contains any files that are derived from source files in `/src`. This file should generally be `.gitignore`d since the source code files are the "authority" and are be checked in. The contents of this folder should be able to be completely deleted ("cleaned") and recreated by running commands in the build tool.

Files in this directory are either:
- **Compiled** from source code (in a compiled language like Java)
- **Generated** from source code. These files can also be source code files that are then subsequently compiled (and should go in `/target/generated-sources`). For code interacting with a relational database, try to use tools like [jOOQ](https://www.jooq.org/) and [pwiz](http://docs.peewee-orm.com/en/latest/peewee/playhouse.html#pwiz) to generate source code from DDL files so that your database model is only declared once (in `/src/sql` or `/src/main/sql`) instead of manually having to keep DDL SQL and data access code in sync.
- **Packaged** for final distribution/deployment