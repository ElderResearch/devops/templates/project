# Source Code

This folder should contain any source code files and any tests that exercise that source code. The code should be organized into one or more packages/libraries/modules instead of a loose collection of files. How the code is organized beneath this folder should be driven by the language and expectation of the packaging/build tool(s) being used.

Source code should be **well documented** (docstrings, Javadocs), well organized, using **logging**, have **tests**, and conform to established **code styles/guidelines**.

| :information_source: Scripts should be organized into a package or moved to [`/exp`](../exp) |
| -- |
| A package can be a single code file. It's better to set up the organization that allows for correct dependency management and deployment/delivery up front. |

## Source Layout/Structure

The following are conventions you should follow, as illustrated by an example project called "Elderberry" consisting of 1) generating features from data in a relational database, 2) training a model and then 3) performing ongoing inference or scoring with that model[^1].

### Single package layout

Most repositories will (and ideally, should) consist of a single package. Our example Elderberry project team started out this way, with just a single repository to contain all the code for the modeling process. 

#### Java (Maven)
```
/elderberry                    Root project folder with pom.xml
   /src/main/java              Java classes organized by package
   /src/main/sql               DDL files and other SQL scripts
   /src/main/resources         Non-code files that should be on the classpath
   /src/test/java              Unit tests
   /src/test/resources         Non-code test files that should be on the classpath
   /target/apidocs             Generated Javadocs
```

#### Python
```
/elderberry                    Root project folder
   /src                        Python package with setup.py, requirements.txt, etc.
      /docs                    Generated Python docs
      /elderberry              Python source code with __init__.py
      /tests                   Unit tests
```

#### R
```
/elderberry                    Root project folder
   /src/elderberry             R package with NAMESPACE, DESCRIPTION, etc.
      /inst                    DDL files and other SQL scripts
      /man                     Generated R manual
      /R                       R source code for the core package
      /tests                   Unit tests
```

### Multiple packages layout

Soon the Elderberry team realizes they want to refactor shared feature creation code to a separate package on which both the training and scoring process could depend.

#### Java (Maven)

For more information on a multi-module build, see [this tutorial](https://www.baeldung.com/maven-multi-module).
```
/elderberry                    Root project folder with parent pom.xml
   /core                       Module for the "core"/shared code with child pom.xml
      /src/main/java           Java classes organized by package
      /src/main/sql            DDL files and other SQL scripts
      /src/main/resources      Non-code files that should be on the classpath
      /src/test/java           Unit tests
      /src/test/resources      Non-code test files that should be on the classpath
      /target/apidocs          Generated Javadocs
   /train                      Module for the training process, which depends on core
      ...                      Same standard Maven layout as above
   /score                      Module for the scoring process, which depends on core
      ...                      Same standard Maven layout as above
```

#### Python
```
/elderberry                    Root project folder
   /src/elderberry_core        Python package with setup.py, requirements.txt, etc.
      /docs                    Generated Python docs
      /eb_core                 Python module with __init__.py
      /tests                   Unit tests
   /src/elderberry_train       Python package for the training process, which depends on core
      ...                      Same standard Python package layout as above
   /src/elderberry_score       Python package for the scoring process, which depends on core
      ...                      Same standard Python package layout as above
```

#### R
```
/elderberry                    Root project folder
   /src/elderberry.core        R package for the "core"/shared code (e.g. feature creation)
      /inst                    DDL files and other SQL scripts
      /man                     Generated R manual
      /R                       R source code for the core package
      /tests                   Unit tests
   /src/elderberry.train       R package for the training process, which depends on core
      ...                      Same standard R package layout as above
   /src/elderberry.score       R package for the scoring process, which depends on core
      ...                      Same standard R package layout as above
```

| :information_source: Deploy packages to [Artifactory](https://elderresearch.jfrog.io/) |
| -- |
| We strongly recommend deploying your source code as a package to our artifact repository, JFrog Artifactory. It supports Conda, PyPi, CRAN, Docker images, Ruby gems, GitLFS, Maven, and NPM, This encourages thinking about modularity, reuse, maintainability, versioning, and other software development principles. |

[^1]: This assumes that the project team decided to (or was forced to) put all 3 components in the same repository instead of splitting them up across multiple repositories. Splitting into separate repositories is generally preferable to putting multiple packages in the same repository, though the latter makes sense when the packages are tightly coupled, with many shared dependencies, and should be versioned together.