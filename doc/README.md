# Documentation Files

This folder holds any additional documentation about your codebase. To keep the root of the directory clean, the root should only contain the `README`, but it can link to additional files, documents, and images in this directory.

| :information_source: Markdown is the preferred file format for documentation |
| --- |
| Markdown is easy to write, easy to read, easy to version control, and is rendered automatically by GitLab and GitHub in the browser |