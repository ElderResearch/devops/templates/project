# Configuration Files

This folder contains configuration files, which are important because:
- **Key parameters** that affect the code (whether it's a model training process, a GUI, an ETL process, etc.) should be easy to find, easy to change, and centralized (only have to change them in one place)
- The same code should run in **various environments** (each contributor's workstation, development vs. production, etc.) without modification. That which _is_ different (a database URL or a file path) should be externalized to configuration.

You should aim to support as many of the following modalities as possible:
- **Environment variables** - these are most expedient in CI/CD and Docker environments and can be a good way of keeping secrets (like a password) out of plain text code and configuration files (though environment variables themselves are not secure).
- **Configuration files** - files are useful to be able to toggle between different environments easily by pointing to different files, as well as more complicated configuration that goes beyond a few key-value pairs. Some configuration files allow specifying an environment variable to use in lieu of the value in a file if one is defined.
- **Command-line arguments** - command-line parameters are well suited for ad-hoc runs or parameters that vary while scripting/orchestrating (for example, if an ETL script needs a date range, a command line argument might be most appropriate so that multiple runs can be started over different date ranges in parallel and without changing files or environment variables). If you use a configuration file, you should support a single command-line argument that specifies which file to use. Always use a robust command-line argument parsing library for your platform (like [Click](https://click.palletsprojects.com/), [JCommander](https://jcommander.org/) or [optparse](https://cran.r-project.org/web/packages/optparse/))

As far as which format to use for your files:
- [**YAML**](https://yaml.org/) should be your first choice whenever possible. It is a superset of JSON and has emerged as the standard for configuration files across many programming languages and use cases. Most cloud-based and CI/CD tools use YAML for configuration. It is very powerful (perhaps too powerful/complex) and readable, supporting comments, unescaped long-form strings, and anchors/aliases.
- In a homogeneous (one language) project/repository, **choose the format that is the most conventional and least friction with your language**. For example, a pure Java codebase worked on by Java developers might opt to use `.properties` files (though [YAML via Jackson](https://github.com/FasterXML/jackson-dataformats-text) is still preferred).
- If your team agrees, you can use [**HOCON**](https://github.com/lightbend/config#using-hocon-the-json-superset) or [**TOML**](https://github.com/toml-lang/toml), though the benefits of either don't seem compelling enough to diverge from the YAML convention.
- Finally, if your configuration requires it, use a file format that supports a formal schema/structure (like XML with XSD). This is only needed if you are encoding chunks of application logic in the file itself (e.g. business rules, system components, domain-specific languages, etc.) where the power of schema-aware tooling (IDEs, code generators, etc.) outweigh the verbosity and inconvenience for humans.

| :information_source: In all cases, uses a package/library that parses (deserializes) and writes (serializes) your configuration data for you. |
| --- |
| Life is too short to write your own parsing code. |