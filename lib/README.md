# Library Files

This folder should not be used often, but is for dependencies that must be packaged with the code and not automatically managed by the build tool. This can come up because:
- The build tool cannot access the open Internet to download the required dependencies, so they need to be locally bundled
- The dependency has a license restriction preventing it from being publicly distributed on the open internet
- The dependency is so old it is no longer available on publicly available repositories
- Native, platform-specific files (`*.so`, `*.dll`) files are required to be on the `PATH`

Even in these above cases, bundling dependencies in the repository itself should be a last resort. If your dependency is not publicly available, deploy it to JFrog Artifactory. If you're behind a firewall, work with a system administrator to set up an on-premise artifact server (or find existing ones, like ADV for COE).

If you are using NPM (`node_modules`) or a PEP-582 compliant Python build tool (`__pypackages__`), you shouldn't (and actually _can't_) coerce them to use `/lib` instead to conform to this convention.

## Maven

Avoid `system` scope (it's been deprecated). Instead, install the file to a local repository under `/lib` and declare it as a repository in your POM. See [this StackOverflow answer](https://stackoverflow.com/a/28762617/5162458) for more information. This makes the build more consistent across different platforms and environments.