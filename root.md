# Root Project Files

<!-- These lists are parsed by the ERI-CLI when creating new repos. Please keep the same structure. -->

The root project directory should have general project information files:
* `CHANGELOG` - A file describing what changed from version to version. [More information](https://keepachangelog.com/)
* `LICENSE` - Licensing terms/terms of use (required for publicly available code). [More information](https://opensource.org/licenses)
* `README` - A file providing a general introduction to the project and how to get set up. [More information](https://www.makeareadme.com/)

Additionally, language- and build-tool specific files that formally define the project:
* `DESCRIPTION` - R
* `MAKEFILE` - General-purpose build tool
* `package.json` - NPM
* `pom.xml` - Maven
* `requirements.txt` - Python

Finally, "dot files" (_which are hidden by default on Linux systems, and is used as a convention to convey they are "system" or "background" files_) like:
* `.gitattributes` - instructs Git how to handle file types
* `.gitignore` - marks file/folder patterns as "invisible" to Git (not version controlled). This template includes a [general-purpose example](.gitignore) across the languages and tooling we often use. Another helpful resource is [gitignore.io](https://gitignore.io/) which can generate a file that is the "union" of many languages, IDEs and frameworks. 
* `.gitlab-ci.yml` - GitLab CI/CD file. [More information](https://docs.gitlab.com/ee/ci/yaml/)
* `.github/workflows/<job>.yml` - GitHub CI/CD file. [More information](https://help.github.com/en/actions/configuring-and-managing-workflows/configuring-a-workflow)
* `.travis.yml` - Travis CI/CD file. Travis has been superseded by GitHub Actions. [More informoation](https://docs.travis-ci.com/user/customizing-the-build)

IDE-Specific files and folders (`/.vscode`, `/.settings`, `/.idea`, etc.) should be `.gitignore`d.